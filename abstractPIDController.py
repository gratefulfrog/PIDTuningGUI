import time

class PIDController(object):
    """Abstrac class defining the minimum functionality to run with the PIDTuningGui:
    you must define the methods:
    * __init__
    * updatePID
    * iter
    * cleanup
    But the methods here can be called via super
    """
    def __init__(self,kValVec,debug):
        self.kValVec = kValVec
        self.debug = debug
        #if not self.debug:
        #    raise NotImplementedError('You need to define an __init__ method!')

    def updatePid(self,index,inc):
        self.kValVec[index] = max(0, self.kValVec[index]+inc)
        #if not self.debug:
        #    raise NotImplementedError('You need to define an updatePid method!')
        
    def iter(self):
        if self.debug:
            print('doing stuff..', self.kValVec[0],self.kValVec[1],self.kValVec[2])
            time.sleep(0.5)

    def cleanup(self):
        if self.debug:
            print('PID controller cleaned up!')

