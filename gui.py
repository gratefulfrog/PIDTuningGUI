#!/usr/bin/python

import threading
import queue
from tkinter import *

from objectTrackingClass import ObjectTracker

class Main(threading.Thread):
    # runs the object tracker,
    # receives K value updates from GUI
    # sends updated K values back to GUI
    def __init__(self,inQ,stopEvt,pidController,gui):
        threading.Thread.__init__(self)
        self.inQ = inQ  # stuff coming from GUI pairs (index, incrementVaue)
        self.stop = stopEvt
        self.pidController = pidController
        self.gui = gui
        # send initial K values to GUI
        for i in range(3):
            self.gui.setK(i,self.pidController.kValVec[i])
        
    def run(self):
        while not self.stop.is_set():
            try:
                (index,incVal) = self.inQ.get(block=False)
                self.pidController.updatePid(index,incVal)
                self.gui.setK(index,self.pidController.kValVec[index])
            except queue.Empty:
                pass
            self.pidController.iter()
        self.pidController.cleanup()

class PIDTuningGUI():
    # displays interactive GUI
    # on button click, sends pair (index, increment) to the Main
    def __init__(self,root,outQ,incValVec):
        self.master = root
        self.outQ = outQ
        self.incValVec = incValVec
        self.master.title("PID Tuner")
        self.nameVec = []
        self.buttonVec = []
        self.labelVec = []
        self.KNameVec = ['Kp','Ki','Kd']
        self.valVec = [StringVar() for i in range(3)]
        for i in range(3):
            self.valVec[i].set(self.KNameVec[i] + ' : ' + str(0))
            self.labelVec.append([])
            for val in self.incValVec:
                self.labelVec[i].append(self.KNameVec[i] + '{0:+}'.format(val))
        self.label = Label(self.master, text='')
        self.label.grid(row=0,columnspan=1+len(self.labelVec[0]))

        self.createButtons()

    def createButtons(self):
        r = 0
        for (vec,lab,ind) in map(lambda v,l,i: (v,l,i),
                                 self.labelVec,
                                 self.valVec,
                                 range(3)):
            self.nameVec.append(Label(self.master, textvariable=lab, anchor='e', width=15))
            r += 1
            c  = 0
            self.nameVec[-1].grid(row=r,column=c,sticky=W+E,padx=5, pady=5)
            c += 1
            for (label,incValInd) in map(lambda l,i: (l,i),
                                         vec,
                                         range(len(self.incValVec))):
                self.buttonVec.append(Button(self.master,
                                             text=label,
                                             command=lambda ind=ind,incValInd = incValInd: self.inc(ind,incValInd)))
                self.buttonVec[-1].grid(row=r,column=c,sticky=W+E,padx=5, pady=5)
                c += 1
        self.close_button = Button(self.master, text="Close", command=self.master.destroy)
        self.close_button.grid(row=r+1,columnspan=1+len(self.labelVec[0]),sticky=W+E,padx=5, pady=5)
        
    def inc(self,i,valInd):
        self.outQ.put((i,self.incValVec[valInd]))

    def setK(self,i,val):
        self.valVec[i].set(self.KNameVec[i] + ' : ' + '{0:8.4f}'.format(val))

def doit(initialKVec,incStepDepth,debug):
    q = queue.Queue()
    root = Tk()
    valVec = [inc for pair in
              [(pow(10,-i),-pow(10,-i)) for i in range(incStepDepth)]
              for inc in pair]
    gui = PIDTuningGUI(root,
                       q,
                       valVec)
    stop = threading.Event()
    pidController = ObjectTracker(initialKVec,debug)
    main = Main(q,stop,pidController,gui)
    main.start()
    root.mainloop()
    stop.set()
    main.join()
    print('Final K values:')
    print('Kp : ' + str(pidController.kValVec[0]))
    print('Ki : ' + str(pidController.kValVec[1]))
    print('Kd : ' + str(pidController.kValVec[2]))
    print ('Clean exit...')

def getInitialValues():
    descriptiveText = 'One tuning method is to first set Ki and Kd values to zero.\n\nIncrease the Kp until the output of the loop oscillates, then the Kp should be\nset to approximately half of that value for a \"quarter amplitude decay\" type response.\n\nThen increase Ki until any offset is corrected in sufficient time for the process. However,\ntoo much Ki will cause instability.\n\nFinally, increase Kd, if required, until the loop is acceptably quick to reach its reference\nafter a load disturbance. However, too much Kd will cause excessive response and overshoot.\n\nA fast PID loop tuning usually overshoots slightly to reach the setpoint more quickly; however,\nsome systems cannot accept overshoot, in which case an overdamped closed-loop system is required,\nwhich will require a Kp setting significantly less than half that of the Kp setting that was\ncausing oscillation.\n(https://en.wikipedia.org/wiki/PID_controller)'

    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--p',
                        type=float,
                        default=1,
                        help='set the initial value of Kp to P')
    parser.add_argument('-i','--i',
                        type=float,
                        default=0,
                        help='set the initial value of Ki to I')
    parser.add_argument('-d','--d',
                        type=float,
                        default=0,
                        help='set the initial value of Kd to D')
    parser.add_argument('-incDepth','-incDepth',
                        type=int,
                        default=4,
                        help='set the value of the depth of increment steps.'
                        'for ex: 4 means that 10^-3 will be the smallest increment step')
    parser.add_argument('-describe', '--describe',
                        action='store_true',
                        help = descriptiveText)
    debug_parser = parser.add_mutually_exclusive_group(required=False)
    debug_parser.add_argument('-debug','--debug',
                              dest='debug',
                              action='store_true',
                              help='sets DEBUG to True')
    debug_parser.add_argument('-no-debug','--no-debug',
                              dest='debug',
                              action='store_false',
                              help='sets DEBUG to False')
    parser.set_defaults(debug=True)

    
    args = parser.parse_args()

    if args.describe:
        print (descriptiveText)
        exit()

    return [args.p,args.i,args.d],args.incDepth,args.debug

if __name__ == '__main__':
    vec,depth,debug =getInitialValues()
    doit(vec,depth,debug)
