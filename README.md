# PID Tuning GUI

If you want to see rather than read: https://youtu.be/clTg4pJZtRA

I used an abstract class to make this code general and usable on **ANY** PID Controlled process!  To use this for another PID controlled process
just look at ```abstractPIDController.py``` and subclass the PIDController class to fit new purposes. Then  instantiate it in place of the
instanciation of ```objectTrackingClass.py```

I took the file ```objectTracking.py``` and made a Class version called ```objectTrackingClass.py```

In that file, I took the old code and wrapped it in a class definition: ```ObjecTracker```.

I was not able to test that class with the real tracking etc. so it will probably have errors that need to be fixed. But you can try that and I can help.

The code runs by default in DEBUG mode, just simulating a PID controller interface but not controlling anything.

To run it in **real** mode:  ```$ ./gui.py --no-debug```

The GUI is contained in the ```gui.py```

That file contains some DEFAULT definitions:
- ```debug=True```   a boolean that controls the display of debugging messages and activates the real code when ```False```
- ```K values Kp=1 Ki=0 Kd=0``` the initial values of the Kp,Ki,Kd coefficients
- ```incStepDepth=4```  smallest increment step size = +/-10^-(incStepDepth-1)

I am running Linux and I have no idea how this works on Windows..  Let me know!

To execute the gui:

```
$ chmod a+x gui.py
$ ./gui.py
('doing stuff..', 1, 0, 0)   # these messages are only in the DEBUG version
('doing stuff..', 1, 0, 0)
('doing stuff..', 1, 0, 0)
('doing stuff..', 1, 0, 0)
('doing stuff..', 1, 0, 0)
('doing stuff..', 1, 0, 0)
PID controller cleaned up!
Final K values:
Kp : 1
Ki : 0
Kd : 0
Clean exit...
```

Alternatively, a **real** example from the command line

```
$ ./gui.py -p 2 -i 0.5 -d .1 -no-debug	
```

# Architecture

<img src="Activities.png">

The program is started from ```gui.py```. The function ```doit()```:
- creates the ```PIDTuningGui``` using python's built-in ```Tkinter``` framework
- creates a ```Main``` thread to run the ```objectTracker```
- creates a synchronized ```queue``` to allow communication from the ```PIDTuningGui``` to the ```objectTracker``` without interruptions or conflict

The ```PIDTuningGui``` displays buttons to allow the user to modify the PID K values while the ```objectTracker``` is running. Updated values are enqueued by the ```PIDTuningGui```. 
Note: that updates resulting in negative K values will be rejected and the K value will be set to zero. 

<img src="hmi.png">

The ```Main``` thread runs a loop:
- check the ```queue``` for updates, if available update the ```objectTracker's``` PID and the GUI display
- run the ```objectTracker``` one time

This sequencing allows the ```objectTracker``` to run uninterrupted and ensures that PID K value updates will never occur during PID computation. Thus no incoherent computation will take place.

Shutting down the ```PIDTuningGui```, cleanly shuts down the ```objectTracker``` as well as the ```Main``` thread, and leaves the system in a proper state.

