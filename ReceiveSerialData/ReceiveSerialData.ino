/*http://forum.arduino.cc/index.php?topic=396450
  From forum.arduino Robin 2
  Modified for this project: receiving data from python serial.
  Data is 13 chars long with start and end markers <,>.
  3 integers are parsed with ';' as separation marker
  Example 5 - Receive with start- and end-markers combined with parsing

  Adjusted to use input data and move steppers to given location
*/

#include <AccelStepper.h>

const byte numChars = 64;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

// variables to hold the parsed data
int intX = 0;
int intY = 0;
int intR = 0;
int deltaX = 0; // Used to fill the delay gap of the PID response
const byte dirX = 13;
const byte pulX = 12;

AccelStepper stepperX(1, pulX, dirX);

const int speedx = 50000;
const int accelx = 1000;


boolean newData = false;

//============

void setup() {
  SerialUSB.begin(2000000);
  delay(2000);
  SerialUSB.println("Native Serial available!");

  stepperX.setMaxSpeed(speedx);
  stepperX.setAcceleration(accelx);
  stepperX.setCurrentPosition(0);
}

//============

void loop() {
  recvWithStartEndMarkers();
  if (newData == true) {
    strcpy(tempChars, receivedChars);
    // this temporary copy is necessary to protect the original data
    //   because strtok() used in parseData() replaces the commas with \0
    parseData();
    showParsedData();
    newData = false;
  }
  if (stepperX.distanceToGo() != 0) {
    stepperX.run();
  }
}

//============

void recvWithStartEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (SerialUSB.available() > 0 && newData == false) {
    rc = SerialUSB.read();

    if (recvInProgress == true) {
      if (rc != endMarker) {
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    }

    else if (rc == startMarker) {
      recvInProgress = true;
    }
  }
}

//============

void parseData() {      // split the data into its parts

  char * strtokIndx; // this is used by strtok() as an index

  strtokIndx = strtok(tempChars, ";");     // get the first part - the string
  intX = atoi(strtokIndx); // copy it to intX

  strtokIndx = strtok(NULL, ";"); // this continues where the previous call left off
  intY = atoi(strtokIndx);     // convert this part to an integer

  strtokIndx = strtok(NULL, ";");
  intR = atoi(strtokIndx);     // convert this part to a float

}

//============

void showParsedData() {
  //  deltaX = stepperX.speed();
  //  deltaX = deltaX * 0.01;
  //  stepperX.move(intX + deltaX);
  stepperX.move(intX);
  
  //  Used for debugging
  //  SerialUSB.print("X: ");
  //  SerialUSB.print(intX);
  //  SerialUSB.print("  Y: ");
  //  SerialUSB.print(intY);
  //  SerialUSB.print("  R:  ");
  //  SerialUSB.println(intR);

  //  SerialUSB.print(intX);
  //  SerialUSB.print("   ");
  //  SerialUSB.println(deltaX);
}
