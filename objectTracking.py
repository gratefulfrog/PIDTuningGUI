
# Import the necessary packages
import numpy as np
import imutils
import cv2
import serial
import math
import time
import PID

#Initialising PID
P =1.5
I = 0
D = 0.015

pid = PID.PID(P, I, D)
setpoint = 10   # not 0 because the piston and camera center are not alligned
pid.SetPoint = setpoint
pid.setSampleTime(0.0001)

# Define the lower and upper boundaries of the
# blue light (circle) in the HSV color space, then initialize the
# # Blue range got form the actual blue wheelplate:
blueLower = (87, 218, 100)
blueUpper = (111, 255, 255)

# Yellow marker test:
# blueLower = (0, 119, 155)
# blueUpper = (42, 255, 255)

# Make connection with Arduino
arduino = serial.Serial('COM5 ',2000000, timeout=0.0001)
time.sleep(2)

# initialize the webcam
camera = cv2.VideoCapture(1) # 1 for external usb-cam, 0 for webcam
camera.set(cv2.CAP_PROP_AUTOFOCUS, 0)
width = camera.set(cv2.CAP_PROP_FRAME_WIDTH,960)
height = camera.set(cv2.CAP_PROP_FRAME_HEIGHT,720)

# init array for calculating mean gap of camera and nozzle
posarray = range(10)*0

while True:
    # grab the current frame
    ret, frame = camera.read()

    # blur the image, construct a mask for the color "blue", then perform
    # a series of dilations and erosions to remove any small
    # blobs left in the mask
    frame = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, blueLower, blueUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    #cv2.imshow("Transformed image", mask)
    canny = cv2.Canny(mask, 50, 150)
    #cv2.imshow("Edges image", canny)

    #Select the contours of the transformed image
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)[-2]
    #Draw crosshair of the camera center
    cv2.line(frame, (int(width / 2 - 20), int(height / 2)), (int(width / 2 + 20), int(height / 2)), (255, 0, 0), 1)
    cv2.line(frame, (int(width / 2), int(height / 2 - 20)), (int(width / 2), int(height / 2 + 20)), (255, 0, 0), 1)

    center = None

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        # only proceed if the radius meets a minimum size
        if radius > 100:
            # add the gap between piston and nozzle to the last element of the array
            posarray.append(x-width/2+setpoint)
            # draw the circle and centroid on the frame,
            # then update the list of tracked points
            cv2.circle(frame, (int(x), int(y)), int(radius),
                       (0, 255, 255), 2)
            cv2.circle(frame, (int(x), int(y)), 5, (0, 0, 255), -1)


            x =int(x - width/2)
            y = int(y - height/2)
            radius = int(radius)
            # calculate mean value of the gap, multiply with factor to increase/reduce influence
            meanX = 4*sum(posarray) / len(posarray)
            pid.update(x)
            x = -1*int(pid.output)

            # influence of gap only when the camera is outside the given range
            if meanX > 40 or meanX < -40:
                x += meanX-10

            del posarray[0] # delete first element of array (oldest term)


            # Send position to arduino
            str1 = '<{};{};{}>'.format(x,y,radius)
            arduino.write(str1.encode())
            # used for debugging
            # print (str1)
            # print(x_)
            # data = arduino.readline()  # Read SerialPort
            # if data:  # When there is data
            #     print (data)

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    # counter = counter + 1
    # if the 'q' key is pressed, stop the loop
    if key == ord("q"):
        break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()



